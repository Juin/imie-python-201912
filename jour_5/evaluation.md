# Evalutation python Campus academy python sequencial

## Exercice 1

Créer un programme permettant de retourner une suite de nombre
basée sur une chaine de caractere.


1 - Faire la somme des positions (dans l'alphabet) des lettres de la chaine (a=0).
Exemple:
```
"benoit" -> 1+4+13+14+8+19 -> 59
```

2 - Puis retourner (ce nombre modulo 60) + 1

59 % 60 + 1 = 60

3 - La suite sera arithmetique commencant par le nombre trouver précédement
    La raison de la suite sera determiné par
    (le nombre trouvé au point N°1 modulo 7) + 1.

raison = 59 % 7 + 1 = 5

premier terme de la suite: 1
la suite est donc:

1
6
11
16
21
....

