import argparse
from pprint import pprint 


def generate_list(input_string):
    """ takes a string in input (input_string)
        returns a liste of 10 elements
        details of implementation are available
        in the evaluation.md file
    """
    seed = 0
    for char in input_string:
        pos = ord(char) - 65
        print("    {0}: {1}".format(char, pos))
        seed += pos
        
    print('sum: {0}'.format(seed))
    first_element = seed % 60 + 1
    raison = seed % 7 + 1
    print('first_element: {0}'.format(first_element))
    print('raison: {0}'.format(raison))
    result = [first_element]
    for i in range(10):
        new_element = result[i]+raison
        result.append(new_element % 60)
    
    return result

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('nom')
    options = parser.parse_args()    
    pprint(options)

    resultat = generate_list(options.nom)
    pprint(resultat)






    
