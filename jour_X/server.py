import socket
import threading
import sys

class ClientHandler(threading.Thread):
    def __init__(self, client_socket, mplex):
        self.client_socket = client_socket
        self.multiplexer = mplex
        super().__init__()
    def run(self):
        while True:
            mes = self.client_socket.recv(255)
            print("mes :%s" % mes)
            print(type(mes))
            self.multiplexer.send_all(mes)

class MultiPlexer():
    def __init__(self):
        self.clients = []
    def add_client(self, cli):
        self.clients.append(cli)
    def send_all(self, msg):
        print(type(msg))
        for cli_soc in self.clients:
            cli_soc.client_socket.send(msg)
            
def run_server(port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', port))
    muplex = MultiPlexer() 
    while True:
        s.listen(5)
        client, address = s.accept()    
        ch = ClientHandler(client, muplex)
        muplex.add_client(ch)
        ch.start()
        
    s.close()


if __name__ == "__main__":
    
    run_server(int(sys.argv[1]))


