# types

## boolean
print("### Les booléens")
True
False
print(
    False,
    type(False)
)
print("False or True =", False or True)
print("False and True =", False and True)
print("True and True =", True and True)

## Integer
# 1, 2, 3 ...
print("\n### les integers")
print(
    1,
    type(1)
)

## Float
# 1.222
print("\n### Les floats")
print(
    1.22,
    type(1.22)
)

print("\n### Les chaines")
print(
    "AAA",
    type("AAA")
)
print("## Substitution")
print("hello {0}, {1}".format(5, 'toto'))
# if __cdt__ :
#    instruction ...
# elif __cdt2__ :
#    instruction
# else :
#    instruction

print("\n### le if")
a = 5
if a > 5:
    print("A est > à 5")
else:
    print("A est > ou = à 5")

# boucle for
# for __var__ in iterable:
#     intruction, 
#     __var__ est disponible

print("\n### Parcour d'une liste:")
mon_iterable = [1,4,7]
for elem in mon_iterable:
    print(elem)
    
print("\n### Parcour d'une autre liste (d'entier):")
for numero in range(10):
    print(numero)


# Liste
print("\n### les listes")
ma_liste = []
ma_liste.append("TOTO")
ma_liste.append(1)
print("ma_liste: {0}".format(ma_liste))
# -- ajouter une element à la fin

dernier_elem = ma_liste.pop()
# -- Retourner le dernier element et
#    l'enlever de la liste

print("dernier element: {0}".format(dernier_elem))
print("ma_liste : {0}".format(ma_liste))
print("longeur de ma_liste {0}".format(len(ma_liste)))



#
#while __condition__ :
    # instruciton
