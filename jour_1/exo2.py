import argparse
from os import path
from pprint import pprint

def get_numer_of_line(filename):
    ret = 0
    with open(filename, 'rt') as f:
        lines = f.read().splitlines()
        ret = len(lines)
    return ret

def run(files=[]):
    file_dico = {}
    for file_name in files:
        print(file_name)
        file_dico[file_name] = get_numer_of_line(file_name)

    total = 0
    for key, value in file_dico.items():
        print(key, value)
        total += value

    print("total", total)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(dest="files",
                        nargs="+",
                        help="file list to get info from")
    args = parser.parse_args()
    
    print(args)
    print(path.abspath(__file__))
    run(files=args.files)
