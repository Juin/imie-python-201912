from datetime import datetime


def run():
    retour = None
    while retour == None:
        retour = input(
            "Quel est votre age?\n")
        try:
            retour = int(retour)
        except ValueError as mon_erreur:
            print("Ceci n'est pas un chiffre")
            retour = None
    if retour < 18:
        print("Non")
    else:
        print("Oui")
    current_year = datetime.now().year
    birth_year = current_year - retour
    output = "Votre année de naissance: {0}"\
                 .format(birth_year)
    print(output)
    
if __name__ == "__main__":
    run()
