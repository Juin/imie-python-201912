import argparse
from pprint import pprint 
 
def get_file_content(file_name):
    result = ""
    with open(file_name, 'rt') as fic:
        result = fic.read()
    return result    

def get_key(elem):
    return elem[1]

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'file',
        help="file to be processed")
    parser.add_argument(
        '-v',
        action='count',
        help="verbose mode -v: low -vvv: high")
    
    options = parser.parse_args()

    text = get_file_content(options.file)
    
    letters_dict = {}
    for char in text:
        if char not in letters_dict:
            letters_dict[char] = 0
        letters_dict[char] += 1

    ordered_list = list(letters_dict.items())
    ordered_list.sort(reverse=True, key=get_key)
        
    for key, value in ordered_list:
        print("{0}: {1}".format(key, value))
