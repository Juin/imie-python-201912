import argparse
from pprint import pprint 
 
def print_map(width, height):
    for i in range(height + 2):
        for j in range(width + 2):
            if i in [0, height + 1]:
                print(chr(9552), end="")
            elif j in [0, width + 1]:
                print(chr(9553), end="")
            else:
                print(" ", end="")
        print("")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    #parser.add_argument(
    #    'name',
    #    action=['store', 'store_true', 'append', 'count', 'version', 'extend'],
    #    nargs=[1,2, '?', '*', '+'],
    #    default="...",
    #    choices=[],
    #    help="",
    #    required=True,
    #    dest=""
    #)

    parser.add_argument(
        'width', nargs="?", default=10, type=int)
    parser.add_argument(
        'height', nargs="?",default=5, type=int)
    
    options = parser.parse_args()    
    pprint(options)

    print_map(options.width, options.height)
