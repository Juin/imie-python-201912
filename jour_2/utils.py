def is_paire(un_nombre):
    """
    is_paire: retourne vrai si 
              l'argument 'un_nombre' est paire.
    """
    reste = un_nombre % 2
    if reste == 0:
        resultat = True
    else:
        resultat = False
    return resultat


def is_premier(un_nombre):
    """
    is_premier: retourne vrai si le parametre 
                'un_nombre' est premier
    """
    resultat = True
    for i in range(un_nombre-1, 1, -1):
        if un_nombre % i == 0:
            resultat = False
    return resultat
