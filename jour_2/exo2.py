def replace_chars_ij(text_source, chars_replaced):
    resultat = ""

    for i in range(len(text_source)):
        to_add = True
        for j in range(len(chars_replaced)):
            if text_source[i] == chars_replaced[j]:
                to_add = False
        if to_add:
            resultat = resultat + text_source[i]
        
    return resultat



def replace_chars(text_source, chars_replaced):
    resultat = []

    for char in text_source:
        if char not in chars_replaced:
            resultat.append(char)
    
    return "".join(resultat)


if __name__ == "__main__":
    retour = replace_chars_ij("azertyuiop", "tyi")

    
