import sys
from utils import is_paire, is_premier


def main():
    param_ok = True
    try:
        premier_argument_ligne_de_cmd = sys.argv[1]
        arguement_en_integer = int(premier_argument_ligne_de_cmd)
    except IndexError as e:
        print("Vous n'avez pas saisie de param")
        param_ok = False
    except ValueError as e:
        print("Votre parametre n'est pas un nombre")
        param_ok = False

    if param_ok:

        print("Liste:")
        for i in range(arguement_en_integer):
            nombre = i+1
            nombre_is_paire = is_paire(nombre)
            nombre_is_premier = is_premier(nombre)

            str_paire = "impaire"
            if nombre_is_paire:
                str_paire = "paire"

            str_premier = ""
            if nombre_is_premier:
                str_premier = "premier"

            print("{0:>3} -> {1:>7} | {2}".format(
                nombre,
                str_paire,
                str_premier
            ))



if __name__ == "__main__":
    main()
