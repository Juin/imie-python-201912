https://gitlab.com/Juin/imie-python-201912

## Jour 1 - généralitées sur python

* Variable
* Portée
* Fonctions
* Classes
* Listes
* Dictionnaires

### Exo 1

Realiser un programme demandant l'age
de l'utilsateur et sortant oui ou non
(majeur ou non)

Exemple:
```
Votre age:
12
Non
18
Oui
Votre année de naissance: ****
```

Info:
 * Afficher une string: print()
 * Récuperer une entrée
   clavier: input()

### Exo 2

Compter le nombre de ligne de n fichiers

Info:

* Lire un fichier open(<nom_du_ficher>)
* librairie STD, argparse

Exemple:
```
python line_numb.py fich1.txt fich2.txt
fich1.txt: 40
fich2.txt: 60
total: 100
```


## Jour 2

* Révision des éléments du jour 1
* import module et package


### Exo 1

Afficher le liste des nombres entiers dans la console.
jusqu'a un paramétre passé en argument dans la ligne de commande:
```
pyton mon_script.py 10

1
2
...
10
```

Afficher, après chaque nombre, si il est pair ou impair
```
pyton mon_script.py 8
1   impair
2   pair
...
8   pair
```
Ajouté, après chaque nombre, si il est premier ou non  
```
pyton mon_script.py 5
1   impair  premier
2   pair
...
5   pair    premier
```

Gérer les cas d'erreur:
  * le programme est appelé sans parametre
  * le programme est appelé avec un
    paramatre qui n'est pas un nombre




### Exo 2

1 -- implementer en python l'algorithme
     définit dans le fichier:
* jour_2/exo2_algo.txt

2 -- Utiliser la fonction implementé
     ci-avant pour l'appliquer sur le
     contenu d'un fichier. Ecrire le
     fichier resultat en prefixant par
     'out_' le nom du fichier source



## Jour 3

Notions:

* les listes (acces au élément, tri)
* les tuples
* les fonctions (arguments positionnels & arguments nommés, passage par référence)
* l'unpacking
* les dictionaires (fonction sur les dictionnaires, parcourir un dictionnaire avec for)

### Exo 1

- Créer une liste de 100 nombres entiers de 0 à
  100 choisis aléatoirements un à un.
- Afficher les 10 derniers éléments de cette
  liste dans la console
- Ordonner croissant cette liste.
- Créer une fonction:
    * prenant une liste en premier arguement "L",
    * un nombre entier en second argument "A",
    * un autre nombre entier en troisième
      arguement "B"
    * La fonction retournera un tuple
      correspondants aux elements de la liste
      "L" à partir de l'indice "A" jusqu'a
      l'indice "A" + "B" 
- En utilisant l'unpacking et la fonction ci-
  avant. Placer les elements de la liste aux
  indices 55, 56 et 57 dans trois variable nommées
   * val_deb,
   * val_mid,
   * val_fin


## Jour 4

* l'univers python et outils
* rappel des notions précédente

### Exo 1

Ecrire un scripte capable d'ouvrir un fichier et
de compter les occurences de chaque lettre.

Exemple:

```
Fichier:
 - nom: fichier_text.txt
 - contenu: AZZZERTTTTTTYUUIOGGHNB

> python mon_programme.py fichier_text.txt
A: 1
Z: 3
E: 1
R: 1
T: 6
Y: 1
U: 2
I: 1
...

```
 - Faire en sorte que votre programme ignore la casse.
 - Faire en sorte que la sortie du prgramme soit
   ordonné de sorte que les lettres apparaissant
   le plus dans le texte soit affichées au début

Exemple:

```
Fichier:
 - nom: fichier_text.txt
 - contenu: AZZZERTTTTTTYUUIOGGHNB

> python mon_programme.py fichier_text.txt
T: 6
Z: 3
U: 2
A: 1
E: 1
R: 1
Y: 1
I: 1
...
```

## EXO packman

### 1 - Afficher la map, sur laquelle sont disposé aléatoirement les objets que packman peu manger.


* le contour de la map sera représenté avec les caractéres ascii: 179, 208, 191, 192, 217
* les objets mangable par le caractére '*'
* La taille de la grille sera paramètrable

### 2 - utiliser les fleches du clavier pour bouger packaman sur la map et réafficher la map en repositionnant packman

### 3 - utiliser le package socket pour implemtenter le jeu en multi joueur.