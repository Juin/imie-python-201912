import math
import random
from pprint import pprint

if __name__ == "__main__":
    #pprint(dir(math))
    #pprint(dir(random))

    for i in range(10):
        print(random.randint(0,100))

    li = list("#"*20)
    print(li)
    pprint(li)

    dico = {
        "cle1": 1,
        "cle2": {
            "sous_cle1": 3
        },
        "liste":li
    }
    print(dico)
    pprint(dico)
