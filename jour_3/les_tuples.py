from pprint import pprint


if __name__ == "__main__":

    mon_tuple = tuple(range(100))
    print(mon_tuple)

    print("mon_tuple[0]", mon_tuple[0])
    print("mon_tuple[-1]", mon_tuple[-1])
    print("mon_tuple[0:5]", mon_tuple[0:5])


    pprint(dir(mon_tuple))

    print("mon_tuple.count(5)", mon_tuple.count(5))
    print("mon_tuple.index(10)", mon_tuple.index(10))
    #print("mon_tuple.index(100)", mon_tuple.index(100))
