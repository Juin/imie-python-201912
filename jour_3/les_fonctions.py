

def ma_function(argument1, argument2):
    return "A"


def mon_autre_fonction(arg1, arg_nomme1="aze"):
    print(arg1, arg_nomme1)
    return "B"

def fonc_a(*args):
    print(args)

def fonc_b(*args, **kwargs):
    print(args)
    print(kwargs)


    
if __name__ == "__main__":
    print(ma_function)
    print(ma_function("A", 1))

    mon_autre_fonction(1, arg_nomme1="C")

    fonc_a("A")
    fonc_a(1,2,3,4)


    fonc_b(1,2,3,4, momo="bla")
    fonc_b(momo="bla", foo="bar")
