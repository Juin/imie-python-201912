import random


def une_fonction(une_liste, indice_1, indice_2):
    indice_3 = indice_1 + indice_2
    resultat = une_liste[indice_1:indice_3]    
    return tuple(resultat)


if __name__ == "__main__":
    ma_liste = []

    for i in range(100):
        nbr = random.randint(0, 100)
        ma_liste.append(nbr)

    print(ma_liste)
    print(ma_liste[90:100])
    print(len(ma_liste[90:100]))
    ma_liste.sort()
    print(ma_liste)
    
    lst = list("abcdefgh")
    print(lst)
    print(une_fonction(lst, 2,3))
    print(une_fonction(list("AZERTYUIO"), 2,3))
